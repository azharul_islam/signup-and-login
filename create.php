<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<meta name="description" content="This is a e-dairy for students"/>
	<meta name="keywords" content="e-dairy, student dairy">
	<meta name="author" content="Leading University, Sylhet"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="create.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js">
	<link rel="icon" href="dairy.jpg">
	<title>Create Account</title>
	</head>

	<body>
	<header>Student<br/><span style="display:inline-block; width: 150px;"></span>Dairy</header>
	<div id="body" class="container">
	<form action="create_success.php" method="post">
	<h1 align="center">Create Dairy</h1>
	<label for="First Name">First Name</label>
	<input type="text" class="form-control" placeholder="First Name" name="first">
	<label for="Last Name">Last Name</label>
	<input type="text" class="form-control" placeholder="Last Name" name="last">
	<label for="User Name">User Name</label>
	<input type="text" class="form-control" placeholder="User Name" name="user">
	<label for="email">E-mail</label>
	<input type="email" class="form-control" placeholder="Enter Email" name="email">
	<label>Password</label>
	<input type="password" class="form-control" placeholder="Password" name="pass"><br>
	<label>Your Country</label>
	<select name="country">
	<option value="">Bangladesh</option>
	<option value="">India</option>
	<option value="">Pakistan</option>
	<option value="">America</option>
	<option value="">England</option>
	<option value="">Franch</option>
	<option value="">Italy</option>
	<option value="">Australiya</option>
	<option value="">Canada</option>
	<option value="">Chine</option>
	</select><br/><br/>
	<label>Upload Profile Picture</label>
	<input type="file" name="pic"><br/>
	<br/>
	<button style="border:5px double blue; float:right" name="submit">Create Your Dairy</button>
	</form>
	</div>
	</body>
</html>